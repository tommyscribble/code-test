console.log('log from tabs.js');


//onClick .....
tabMenu = (el, tabId) => {

    //Hide all content blocks
    var contentBlock = document.getElementsByClassName("tab-component__content-block");
    for (i = 0; i < contentBlock.length; i++) {
        contentBlock[i].style.display = "none";
    };
    //Remove active class from all tabs
    var tabItem = document.getElementsByClassName("tab-component__tab-link");
    for (i = 0; i < tabItem.length; i++) {
        tabItem[i].className = tabItem[i].className.replace(" active", "");
    };
    //Add active class to clicked tab and display coressponding content block
    document.getElementById(tabId).style.display = "block";
    el.currentTarget.className += " active";
};
